help () {
            cat <<EOF
You're running WorrNix 1.0!

The package manager is get-app. You can install packages with
get-app install <program>

You can search for packages with the command
get-app search <keyword>
EOF
}

get-app () {
    if [[ $# -lt 2 ]]; then
        print "usage: get-app {install|search} <package>"
        return
    fi

    if [[ ${1} = "install" ]]; then
        if [[ -n ${installed[(r)${2}]} ]]; then
            print "${2} is already installed"
            return
        fi

        if [[ -n ${installable_programs[(rk)${2}]} ]]; then
            print "Installing ${2}"
            sleep 1

            print "Downloading..."
            sleep $((${installable_programs[${2}]} * 2))

            print "Installing..."
            sleep ${installable_programs[${2}]}

            print "Installation successful!"
            installed+=${2}

            achievement_get "Package Pro"
            add_points 10

            if [[ ${2} = "brunoc" ]]; then
                achievement_get "Brunoc?"
                add_points 100
            fi

            if [[ ${#installed} -eq ${#installable_programs} ]]; then
                achievement_get "Package Master"
                add_points 10000
            fi

            touch ${game_chroot}/bin/${2}
        else
            print "No such package ${1}"
        fi
    elif [[ ${1} = "search" ]]; then
        print "Searching..."
        sleep 1

        if [[ ${2} =~ "^\*" ]]; then
            add_points 20
            achievement_get "Close, but not quite"
            return
        fi

        if [[ ${2} = '.*' ]]; then
            add_points 100
            achievement_get "Oh man I got this shit unlocked"
        fi

        for i in ${(k)installable_programs[@]}; do
            if [[ ${i} =~ ${2} ]]; then
                print ${i}
            fi

            sleep 1
        done
    fi
}

wget () {
    if [[ $# -ne 1 ]]; then
        print "usage: wget <URL>"
        return
    fi

    if [[ ! ( ${1} =~ "^http://" ) ]]; then
        print "Must use proper http URL"
        return
    fi

    print "Resolving ${1}..."
    sleep 2
    if [[ ${1} =~ "^http://perlthonby.org/" ]]; then
        achievement_get "Upstream ho!"
        add_points 50
        print "404 File not found"
        print "Try our mirror at http://ibiblio.org/"
    elif [[ ${1} =~ "^http://ibiblio.org/" ]]; then
        print "302 Moved to http://youareinthemetagame.org/"
        add_points 40
    elif [[ ${1} =~ "^http://brunoc.co.jp/brunoc.zip" ]]; then
        add_points 100
        touch brunoc.zip
    else
        print "Could not resolve ${1}"
    fi
}

achievements () {
    print "\nYou have achieved:\n"
    for i in ${achieved[@]}; do
        print "\t${fg_no_bold[red]}$i"
        print "${fg_no_bold[blue]}${achievements[$i]}\n"
    done
    print -n "${fg_no_bold[white]}"
}

perlthonby () {
    cat <<EOF
Perlthonby 0.3

WARNING: You don't have the newest version of Perlthonby! Please install
perlthonby from our web site instead of your package manager!

http://perlthonby.org/perlthonby-1.0.zip
EOF
}

brunoc () {
    cat <<EOF
brunoc 0.1

WARNING: This version of brunoc is incredibly unstable. Please do try the new
number one version interpreter bruonc from page of the home at
brunoc.co.jp/brunoc.zip
EOF

}

editor () {
    achievement_get "Vim? Emacs? Vimacs?"
    if [[ editor_opened -eq 0 ]]; then
        add_points 30
        editor_opened="1"
    fi
    red ${*}
}

unzip() {
    if [[ ${1} != "brunoc.zip" ]]; then
        mkdir "brunoc"
        touch "brunoc/configure"
        valid_commands+="configure"
        cat > "brunoc/README" <<EOF
Brunoc v0.6

Brunoc is best interpreter for Perlthonby. It is used to make awesome code in
language to bytecode and run.

Brunoc is in the betas and may bugs have. Please to be used by programming 
expers who report bugs to upstream. Manny code is based off of the main
perlthonby interpreter code perlthonby.
EOF
    else
        print "${1} is not a valid zip file"
    fi
}

configure() {
    if [[ ${PWD} =~ "brunoc$" && -e "configure" ]]; then
        touch "Makefile"
    else
        print "${0}: command not found"
    fi
}

make() {
    if [[ ! -e "Makefile" ]]; then
        if [[ ${PWD} =~ "brunoc$" ]]; then
            print "No sources to compile"
        else
            print "Don't know how to build for ${2:-all}"
        fi
    else
        print "No Makefile found"
    fi
}
