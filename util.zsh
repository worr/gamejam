add_points() {
    points=$((${points} + ${1}))
}

achievement_get() {
    if [[ -n ${achieved[(r)${1}]} ]]; then
        return
    fi

    print "\n\n******************************************************************************"
    print "${fg_no_bold[green]}Saluatoratations! ${fg_no_bold[white]}Achievement ${fg_no_bold[green]}Get!"
    print "${fg_no_bold[red]}${1}\n${fg_no_bold[blue]}${achievements[$1]}${fg_no_bold[white]}"
    print "******************************************************************************\n\n"

    achieved+=(${1})
}

sanitize_paths() {
    if [[ ${paths[${1}]} =~ "^\-" ]]; then
        return
    fi

    if [[ ${paths[${1}]} = ".." ]]; then
        if [[ ${current_directory} = "/" ]]; then
            paths[${1}]="${game_chroot}/"
            return
        fi

        paths[${1}]="${game_chroot}${current_directory%%/[a-zA-Z0-9]*}"
        return
    elif [[ ${paths[${1}]} = "." ]]; then
        paths[${1}]="${game_chroot}${current_directory}"
        return
    fi

    paths[${1}]=${paths[${1}]//\./}
    if [[ ${paths[${1}]} =~ "^/" ]]; then
        paths[${1}]="${game_chroot}${paths[${1}]}"
    else
        paths[${1}]="${game_chroot}/${current_directory}/${paths[${1}]}"
    fi
    paths[${1}]=${paths[${1}]//\/\//\/}
}

TRAPINT() {
    exit_function
}

TRAPEXIT() {
    exit_function
}

exit_function() {
    print "\n\nYou ${fg_no_bold[red]}failed${fg_no_bold[white]}! Congratulations!"
    print "You earned ${points} points!"
    rm -rf ${game_chroot}
    exit
}
