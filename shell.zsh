#!/usr/bin/env zsh

. ../util.zsh 
. ../commands.zsh 
. ../vars.zsh

validate() {
    if [[ -n ${valid_commands[(r)${1}]} || -n ${installed[(r)${1}]} ]]; then
        if [[ ${1} = "pwd" ]]; then
            print $current_directory
        elif [[ -n ${pathy_commands[(r)${1}]} ]]; then
            if [[ ${#} -eq 1 ]]; then 
                ${1} "${game_chroot}/${current_directory}"
                return
            fi

            local -x paths
            paths=(${@[2,-1]})

            for i in {1..${#paths}}; do
                sanitize_paths $i
            done


            if [[ "$*" =~ "rm -[rf]{2} /" ]]; then
                achievement_get "Fuck this game"
                add_points -10000000
                rm -rf "${game_chroot}/*"
                return
            fi

            ${1} ${paths[@]}
            if [[ ${1} = "cd" ]]; then
                current_directory=${paths[1]#${game_chroot}}
                if [[ ${current_directory} = ${game_chroot} || -z ${current_directory} ]]; then
                    current_directory="/"
                fi
                current_directory=${current_directory//\/\//\/}
            fi
        else 
            $*
        fi
    else
        print "${1}: command not found"
    fi
}

game_chroot="${PWD}"
autoload colors && colors
print -n "hellsh$ "
while read; do
    if [[ -n ${shells[(r)${REPLY}]} ]]; then
        achievement_get "Fuck yo shell"
        add_points -10
    fi

    validate ${=REPLY}
    print -n "hellsh$ "
done
