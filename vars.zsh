# Programs installable through the "package manager"
typeset -A installable_programs
installable_programs=(
    editor "10" 
    perlthonby "300" 
    wget "10" 
    brunoc "600" 
    unzip "10" 
    make "20"
)

installed=()

valid_commands=(
    ls
    mkdir
    cd
    touch
    rmdir
    rm 
    cat 
    get-app 
    achievements 
    help 
    pwd
)

pathy_commands=(
    ls
    mkdir
    cd
    touch
    rmdir
    rm
    cat
)

# We need to keep track of the player's points
points=0

# Achievements?!?!?!?!?!
typeset -A achievements
achievements=(
    "Package Pro" "You successfully installed a package. Good fucking job."

    "Package Master" "You installed all of the packages!"

    "Vim? Emacs? Vimacs?" "You opened your editor for the very first time!"

    "Fuck yo shell" "You tried to use a 'real' shell."

    "Man behind the curtain" "You realized that all of the programs in bin are
    blank!"

    "Upstream ho!" "You bothered to try the upstream URL! Too bad it 404'd!"

    "Brunoc?" "You found out about the alternative interpreter"

    "Close, but not quite" "You tried to use globbing in a get-app search"

    "Oh man I got this shit unlocked" "You listed all of the packages"

    "Fuck this game" "rm -rf / - like a boss"
)

achieved=()
shells=(
    bash
    ksh
    zsh
    tcsh
    csh
    fish
)

# Game state
current_directory="/"
game_chroot=""
editor_opened="0"
