# Game Jam
My game is a commentary on the process of program creation and the bullshit
required to get your environment ready to program.

## How to play
Make sure that main.zsh is executable, and run the shit out of it.

## Tips for the UNIXtards

* /bin often contains all of your installed programs
* package managers are used to install programs
* regexes are useful in searches
* wget is used to download things from the internet
