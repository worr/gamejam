#!/usr/bin/env zsh

game_chroot="${PWD}/chroot_${USER}"

if [[ -d ${game_chroot} ]]; then
    rm -rf ${game_chroot}
fi
cp -r chroot_base ${game_chroot}

# Now let's start the game!
cat <<END
Hey! You are a game programmer in CSH Game Jam 2012. You came up with a
totally radical idea, and you're ready to create an awesome game! You're even
going to use a cool new programming language - Perlthonby! 

Perlthonby is a super new programming language, so it doesn't have any
libraries, or packages written for it yet. But that's not a problem for a
super smart programmer like you!

Unfortunately, you didn't set up your environment ahead of time. So you best
get started with that part right away!

Points will be tallied when you give up.

Good luck! Ctrl+D to exit. There is no save. There is no help. There is no
winning.
END

cd ${game_chroot}

exec ../shell.zsh
